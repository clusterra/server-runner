/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.server;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by Denis Kuchugurov
 * on 01/08/15 14:58.
 */
public class ApplicationVersion {

    private final static Logger LOGGER = LoggerFactory.getLogger(ApplicationVersion.class);

    @Autowired
    private ApplicationContext applicationContext;

    public void printVersions() throws IOException {

        String name = "classpath*:/META-INF/MANIFEST.MF";
        Resource[] resources = applicationContext.getResources(name);

        for (Resource resource : resources) {

            Properties props = new Properties();
            props.load(resource.getInputStream());

            String moduleGroup = props.getProperty("Module-Group");
            String moduleName = props.getProperty("Module-Name");
            String moduleVersion = props.getProperty("Implementation-Version");
            String buildNumber = props.getProperty("Build-Number");
            if ("com.clusterra".equalsIgnoreCase(moduleGroup)) {
                LOGGER.info("{}:{}({}):{}", moduleGroup, moduleVersion, StringUtils.abbreviate(buildNumber, 8), moduleName);
            }

        }

    }

}
